import cv2

from naogi import NaogiModel, FileRenderer
from basicsr.archs.rrdbnet_arch import RRDBNet
from realesrgan import RealESRGANer

class ImageRenderer(FileRenderer):
  @classmethod
  def render(cls, image):
    _, im_buf_arr = cv2.imencode(".jpg", image)
    bytes_io = io.BytesIO(im_buf_arr)
    bytes_io.seek(0)
    return super().render(bytes_io, content_type='image/jpg')

class Model(NaogiModel):
  def load_model(self):
    _model = RRDBNet(num_in_ch=3, num_out_ch=3, num_feat=64, num_block=23, num_grow_ch=32, scale=4)
    model_path = os.path.join('./RealESRGAN_x4plus.pth')
    netscale = 4

    self.outscale = 4

    self.model = RealESRGANer(
      scale = netscale,
      model_path = model_path,
      model = _model,
      tile = args.tile,
      tile_pad = args.tile_pad,
      pre_pad = args.pre_pad,
      half = not args.fp32,
      gpu_id = 0
    )

  def predict(self):
    output, _ = self.model.enhance(self.image, outscale = self.outscale)
    return output

  def prepare(self, params):
    self.image = cv2.imread(params['file'], cv2.IMREAD_UNCHANGED)

  def renderer(self):
    return ImageRenderer

